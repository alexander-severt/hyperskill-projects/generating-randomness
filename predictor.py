import re
import random

learn_target_len = 100
starting_balance = 1000


def get_data():
    print('Print a random string containing 0 or 1:\n')
    learn_string_input = input()
    learn_string = ''.join([i for i in learn_string_input if i in '01'])

    while len(learn_string) < learn_target_len:
        length = len(learn_string)
        print(f'Current data length is {length}, {learn_target_len - length} symbols left')
        print('Print a random string containing 0 or 1:\n')
        learn_string_input = input()
        learn_string += ''.join([i for i in learn_string_input if i in '01'])

    return learn_string


def get_triad_count(learn_string):
    triad_counts = {}
    for num in range(0, 8):
        search_triad = bin(num).split('0b', 1)[1].zfill(3)
        triad_counts[search_triad] = {}

        triad_counts[search_triad][0] = len(re.findall('(?=' + search_triad + '0)', learn_string))
        triad_counts[search_triad][1] = len(re.findall('(?=' + search_triad + '1)', learn_string))

    return triad_counts


def get_prediction(user_string, triads, prediction_length):
    prediction = ''

    for i in range(3, prediction_length):
        triad = user_string[i - 3:i]

        if triads[triad][0] > triads[triad][1]:
            number = '0'
        elif triads[triad][1] > triads[triad][0]:
            number = '1'
        else:
            number = random.choice('01')

        prediction += number

    return prediction


def main():
    print('Please provide AI some data to learn...')
    print(f'The current data length is 0, {learn_target_len} symbols left')

    learn_string = get_data()
    print(f'Final data string:\n{learn_string}\n')

    triad_counts = get_triad_count(learn_string)

    print('You have $1000. Every time the system successfully predicts your next press, you lose $1.\n' +
          'Otherwise, you earn $1. Print "enough" to leave the game. Let\'s go!\n')

    predict_string_input = ''
    balance = starting_balance

    while predict_string_input != 'enough':
        predict_string_input = input('Print a random string containing 0 or 1:\n')
        if predict_string_input == 'enough':
            break
        predict_string_input = ''.join([i for i in predict_string_input if i in '01'])
        length = len(predict_string_input)

        while length < 4:
            predict_string_input = input('Print a random string containing 0 or 1:\n')
            if predict_string_input == 'enough':
                break
            predict_string_input = ''.join([i for i in predict_string_input if i in '01'])
            length = len(predict_string_input)
        predict_nums = get_prediction(predict_string_input, triad_counts, length)
        print(f'predictions:\n{predict_nums}\n')

        correct_nums = len([1 for i in range(0, length-3) if predict_string_input[i+3] == predict_nums[i]])
        wrong_nums = length - 3 - correct_nums
        balance += wrong_nums - correct_nums
        print(f'Computer guessed {correct_nums} out of {length - 3} symbols right '
              f'({round(100 * correct_nums / (length - 3), 2)} %)')
        print(f'Your balance is now ${balance}')

    print('Game over!')


if __name__ == '__main__':
    main()
